#include "threads.h"

void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread love_threads(I_Love_Threads);

	love_threads.join();
}

void printVector(std::vector<int> primes)
{
    for (int n : primes)
    {
        std::cout << n << std::endl;
    }
}

void getPrimes(int begin, int end, std::vector<int>& primes)
{
    bool isPrime = false; 

    // Traverse each number in the interval with the help of for loop 
    for (int i = begin; i <= end; i++) {

        // Skip 0 and 1 as they are neither prime nor composite 
        if (i == 1 || i == 0)
            continue;

        // flag variable to tell if i is prime or not
        isPrime = true;

        // Iterate to check if i is prime or not
        for (int j = 2; j <= i / 2; ++j) {
            if (i % j == 0) {
                isPrime = false;
                break;
            }
        }

        if (isPrime)
            primes.push_back(i);
    }
}

std::vector<int> callGetPrimes(int begin, int end)
{
    std::vector<int> primes;

    // divide to 4 threads
    int slice = (end - begin) / 4;

    std::vector<int> quarterPrimes2;
    std::vector<int> quarterPrimes3;
    std::vector<int> quarterPrimes4;

    auto t1 = std::chrono::high_resolution_clock::now(); // start clock

    // begin threads
    std::thread getQuarterPrimes1(getPrimes, begin, (begin + slice), std::ref(primes));
    std::thread getQuarterPrimes2(getPrimes, (begin + slice + 1), (begin + slice * 2), std::ref(quarterPrimes2));
    std::thread getQuarterPrimes3(getPrimes, (begin + slice * 2 + 1), (begin + slice * 3), std::ref(quarterPrimes3));
    std::thread getQuarterPrimes4(getPrimes, (begin + slice * 3 + 1), end, std::ref(quarterPrimes4));


    // wait for threads to finish
    getQuarterPrimes1.join();
    getQuarterPrimes2.join();
    getQuarterPrimes3.join();
    getQuarterPrimes4.join();

    // join the quarters
    primes.insert(primes.end(), quarterPrimes2.begin(), quarterPrimes2.end());
    primes.insert(primes.end(), quarterPrimes3.begin(), quarterPrimes3.end());
    primes.insert(primes.end(), quarterPrimes4.begin(), quarterPrimes4.end());

    auto t2 = std::chrono::high_resolution_clock::now(); // stop clock 
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count(); // calculate duration of the threads
    std::cout << duration << std::endl; // print duration
 
    return primes;
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
    if (file.is_open()) // check that the file sent was properly open
    {
        bool isPrime = false;

        // Traverse each number in the interval with the help of for loop 
        for (int i = begin; i <= end; i++) {

            // Skip 0 and 1 as they are neither prime nor composite 
            if (i == 1 || i == 0)
                continue;

            // flag variable to tell if i is prime or not
            isPrime = true;

            // Iterate to check if i is prime or not
            for (int j = 2; j <= i / 2; ++j) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime)
            {
                file << "\n" << i << "\n";
            }
        }
    }
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
    // open file
    std::ofstream file;
    file.open(filePath);

    // get the amount of numbers each thread covers
    int slice = (end - begin) / N;
    std::vector<std::thread> multiThread; 

    auto t1 = std::chrono::high_resolution_clock::now(); // start clock

    // begin threads
    for (int i = 0; i < N; i++)
    {
        // get the start of the slice the thread needs to check
        int beginSlice = begin + slice * i; 

        if (i != 0) // the start of each slice (that isn't the first one) needs to start one after  
            beginSlice++;

        // get the end of the slice the thread needs to check
        int endSlice = begin + slice * (i+1); 

        if (i == N - 1) // if we are at the last thread, the end of the slice is the end
            endSlice = end;

        // make thread
        multiThread.push_back(std::move(std::thread(writePrimesToFile, beginSlice, endSlice, std::ref(file))));
    }

    // wait for threads to finish proccesing
    for (int i = 0; i < N; i++)
    {
        multiThread[i].join();
    }

    auto t2 = std::chrono::high_resolution_clock::now(); // stop clock 
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count(); // calculate duration of the threads
    std::cout << duration << std::endl; // print duration
}
